<?php 
global $tekoconfig, $title;

$title = __('Could not connect to the database');

$message =
'<h3>'.__('Hi? Anyone there?').'</h3>'.
'<p>'.__('Could not connect to the database').' <strong>'.$tekoconfig['db']['name'].'</strong>.</p> <p>'.__('Verify your connection data').'</p>';

require_once(APPPATH.'/teko-core/assets/error/index.php');