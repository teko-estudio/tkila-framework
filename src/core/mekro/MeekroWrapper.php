<?php

class MeekroWrapper extends DB
{
    public function __construct($dbuser, $dbpassword, $dbname, $dbhost='localhost', $dbport = 3306)
    {
        parent::$host = $dbhost;
        parent::$port = $dbport;
        parent::$user = $dbuser;
        parent::$password = $dbpassword;
        parent::$dbName = $dbname;
    }

    public function get_var($query)
    {
        return parent::queryFirstField($query);
    }

    public function get_col($query)
    {
        return parent::queryFirstColumn($query);
    }

    public function get_row($query)
    {
        return (object)parent::queryFirstRow($query);
    }

    public function get_results($query)
    {
        return json_decode(json_encode(parent::query($query)));
    }

    /**
     * Método mágico __call() para redirigir llamadas de métodos inexistentes a MeekroDB
     * @throws Exception
     */
    public function __call($method, $arguments)
    {
        parent::__callStatic($method, $arguments);
    }
}