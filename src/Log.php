<?php

namespace Tkila;

/**
 * @method static void debug($message)
 * @method static void info($message)
 * @method static void notice($message)
 * @method static void warning($message)
 * @method static void error($message)
 * @method static void critical($message)
 * @method static void alert($message)
 * @method static void emergency($message)
 */
class Log
{

    private static $dir = false;
    public static function setDirectory($dir)
    {
        static::$dir = $dir;
    }

    public static function __callStatic($name, $arguments)
    {
        if (static::$dir) {
            $path = static::$dir . DIRECTORY_SEPARATOR . "logs/{$name}.log";
            $dir = dirname($path);
            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
            }

            if (count($arguments) > 0) {
                [$message] = $arguments;
                $timestamp = date('Y-m-d H:i:s');
                $formattedMessage = "[{$timestamp}] {$message}\n";
                file_put_contents($path, $formattedMessage, FILE_APPEND | LOCK_EX);
            }
        }
    }
}
