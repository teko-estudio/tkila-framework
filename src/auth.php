<?php

//Register
use Delight\Auth\EmailNotVerifiedException;
use Delight\Auth\InvalidEmailException;
use Delight\Auth\InvalidPasswordException;
use Delight\Auth\InvalidSelectorTokenPairException;
use Delight\Auth\NotLoggedInException;
use Delight\Auth\TokenExpiredException;
use Delight\Auth\TooManyRequestsException;
use Delight\Auth\UnknownUsernameException;
use Delight\Auth\UserAlreadyExistsException;

function tk_register($email, $pass, $role = 'user', $username = null, $verify = false, $extra = []){
    global $tekodb, $tekoauth, $token;
    try {
        if($verify){
            $id = $tekoauth->register($email, $pass, $username, function ($selector, $token) {
                $GLOBALS['token'] = urlencode(base64_encode(json_encode(compact('selector', 'token'))));
            });
        } else {
            $id = $tekoauth->admin()->createUser($email, $pass, $username);
        }
        if(count($extra)){
            $extra['role'] = $role;
            $tekodb->update('users', $extra, compact('id'));
        } else {
            $tekodb->update('users', compact('role'), compact('id'));
        }
        return ($verify) ? array('error' => FALSE, 'id'=> $id, 'token' => $token) : array('error' => FALSE, 'id' => $id);
    }
    catch (InvalidEmailException $e) {
        return array('error' => TRUE, 'type' => 'email', 'message' => __('Invalid e-mail'));
    }
    catch (InvalidPasswordException $e) {
        return array('error' => TRUE, 'type' => 'password', 'message' => __('Invalid password'));
    }
    catch (UserAlreadyExistsException $e) {
        return array('error' => TRUE, 'type' => 'username_exists', 'message' => __('The username already exists'));
    }
    catch (TooManyRequestsException $e) {
        return array('error' => TRUE, 'type' => 'too_many_requests', 'message' => __('Too many requests, try again in %s', gmdate("H:i:s", $e->getCode())));
    }
    catch (Exception $e) {
        return array('error' => TRUE, 'type' => 'unknow', 'message' => __('Unknow error: %s', $e->getMessage()), 'e' => $e);
    }
}

//E-mail confirm
function tk_confirm($token){
    global $tekoauth;
    try {
        $partes = json_decode(base64_decode(urldecode($token)));
        $tekoauth->confirmEmail($partes->selector, $partes->token);
        return array('error' => FALSE);
    }
    catch (InvalidSelectorTokenPairException $e) {
        return array('error' => TRUE, 'type' => 'invalid_token', 'message' => __('Invalid token selector'));
    }
    catch (TokenExpiredException $e) {
        return array('error' => TRUE, 'type' => 'expired_token', 'message' => __('Token expired'));
    }
    catch (TooManyRequestsException $e) {
        return array('error' => TRUE, 'type' => 'too_many_requests', 'messages' => __('Too many requests, try again in %s', gmdate("H:i:s", $e->getCode())));
    }
    catch (Exception $e) {
        return array('error' => TRUE, 'type' => 'unknow', 'message' => __('Unknow error: %s', $e->getMessage()), 'e' => $e);
    }
}

//password Perdida
function tk_recover($email){
    global $tekoauth, $token;
    try {
        $token = '';
        $tekoauth->forgotPassword($email, function ($selector, $token) {
            $GLOBALS['token'] = urlencode(base64_encode(json_encode(compact('selector', 'token'))));
        });
        return array('error' => FALSE, 'token' => $token);
    }
    catch (InvalidEmailException $e) {
        return array('error' => TRUE, 'type' => 'email', 'message' => __('Invalid e-mail'));
    }
    catch (TooManyRequestsException $e) {
        return array('error' => TRUE, 'type' => 'too_many_requests', 'message' => __('Too many requests, try again in %s', gmdate("H:i:s", $e->getCode())));
    }
    catch (Exception $e) {
        return array('error' => TRUE, 'type' => 'unknow', 'message' => __('Unknow error: %s', $e->getMessage()), 'e' => $e);
    }
}

//Reset Password
function tk_reset_password($token, $password){
    global $tekoauth;
    try {
        $partes = json_decode(base64_decode(urldecode($token)));
        $tekoauth->resetPassword($partes->selector, $partes->token, $password);
        return array('error' => FALSE);
    }
    catch (InvalidSelectorTokenPairException $e) {
        return array('error' => TRUE, 'type' => 'invalid_token', 'message' => __('Invalid token selector'));
    }
    catch (TokenExpiredException $e) {
        return array('error' => TRUE, 'type' => 'expired_token', 'message' => __('Token expired'));
    }
    catch (InvalidPasswordException $e) {
        return array('error' => TRUE, 'type' => 'password', 'message' => __('Invalid password'));
    }
    catch (TooManyRequestsException $e) {
        return array('error' => TRUE, 'type' => 'too_many_requests', 'message' => __('Too many requests, try again in %s', gmdate("H:i:s", $e->getCode())));
    }
    catch (Exception $e) {
        return array('error' => TRUE, 'type' => 'unknow', 'message' => __('Unknow error: %s', $e->getMessage()), 'e' => $e);
    }
}

//Change current user password
function tk_change_current_password($antigua, $nueva){
    global $tekoauth;
    try {
        $tekoauth->changePassword($antigua, $nueva);
        return array('error' => FALSE);
    }
    catch (NotLoggedInException $e) {
        return array('error' => TRUE, 'type' => 'not_logged_in', 'message' => __('Not logged in'));
    }
    catch (InvalidPasswordException $e) {
        return array('error' => TRUE, 'type' => 'password', 'message' => __('Invalid password'));
    }
    catch (Exception $e) {
        return array('error' => TRUE, 'type' => 'unknow', 'message' => __('Unknow error: %s', $e->getMessage()), 'e' => $e);
    }
}

//Change password by username
function tk_change_password($password, $username = FALSE){
    global $tekodb;
    $id = ($username) ? $username : tk_id();
    $password = password_hash($password, PASSWORD_DEFAULT);
    $res = $tekodb->update('users', compact('password'), compact('id'));
    if($res !== FALSE){
        return array('error' => FALSE, 'message' => __('Password changed successfully'));
    } else {
        return array('error' => TRUE, 'message' => __('An error occurred when changing the password'));
    }
}

//Change role
function tk_change_role($id, $role){
    global $tekodb;
    $tekodb->update('users', compact('role'), compact('id'));
    return array('error' => FALSE, 'message' => __('Role changed successfully'));
}

//Login
function tk_login($email, $pass, $remember = FALSE){
    global $tekoauth;
	$remember = ($remember) ? ((int) (60 * 60 * 24 * 365.25)) : null;
    try {
    	if(filter_var($email, FILTER_VALIDATE_EMAIL)){
			$tekoauth->login($email, $pass, $remember);
		} else {
			$tekoauth->loginWithUsername($email, $pass, $remember);
		}
        return array('error' => FALSE, 'id'=>$tekoauth->getUserId());
    }
    catch (InvalidEmailException $e) {
        return array('error' => TRUE, 'type' => 'email', 'message' => __('Invalid e-mail'));
    }
    catch (InvalidPasswordException $e) {
        return array('error' => TRUE, 'type' => 'password', 'message' => __('Invalid password'));
    }
    catch (EmailNotVerifiedException $e) {
        return array('error' => TRUE, 'type' => 'email_not_verified', 'message' => __('Your user has not yet been verified, check your inbox'));
    }
    catch (TooManyRequestsException $e) {
        return array('error' => TRUE, 'type' => 'too_many_requests', 'message' => __('Too many requests, try again in %s', gmdate("H:i:s", $e->getCode())));
    }
	catch(UnknownUsernameException $e) {
		return array('error' => TRUE, 'type' => 'unknow_user', 'message' => __('Incorrect username or password'));
	}
    catch (Exception $e) {
        return array('error' => TRUE, 'type' => 'unknow', 'message' => __('Unknow error: %s', $e->getMessage()), 'e' => $e);
    }
}

//Log out
function tk_logout(){
    global $tekoauth;
    $tekoauth->logout();
}

//Get current user id
function tk_id(){
    global $tekoauth;
    if ($tekoauth->isLoggedIn()) {
        return $tekoauth->getUserId();
    }
    else {
        return 0;
    }
}

//Alias
function get_current_user_id(){ return tk_id(); }

//Get current user
function tk_current_user(){
    global $tekodb;
    $id = tk_id();
    return $tekodb->get_row("SELECT * FROM users WHERE id={$id}");
}

//Get current user role
function get_current_user_role(){
    global $tekodb;
    return $tekodb->get_var("SELECT role FROM users WHERE id=".tk_id());
}

//Check if password is correct
function tk_check_password($password, $id_user = 0){
    global $tekodb;
    $id_user = ($id_user) ? $id_user : tk_id();
    if($id_user){
        $passwordInDatabase = $tekodb->get_var("SELECT password FROM users WHERE id = {$id_user}");
        return password_verify($password, $passwordInDatabase);
    } else {
        return false;
    }
}