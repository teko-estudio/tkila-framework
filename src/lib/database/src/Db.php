<?php

namespace Tkila\Database;

class Db extends Model {

    /**
     * Creates a new instance of the current class and sets the table name.
     *
     * @param string $name The name of the table.
     * @return static The newly created instance of the class.
     */
    public static function table(string $name) {
        $instance = new static();
        $instance->setTable($name);
        return $instance;
    }

    /**
     * Sets the table name for the database connection.
     *
     * @param string $name The name of the table.
     */
    private function setTable(string $name) {
        $this->table = $name;
        $this->initSelect();
    }


    /**
     * Commits the current transaction by executing the 'COMMIT' query on the global $tekodb database connection.
     *
     * @throws Some_Exception_Class If the 'COMMIT' query fails to execute.
     */
    public static function commit() {
        global $tekodb;
        $tekodb->query('COMMIT');
    }

    /**
     * Rolls back the current transaction by executing the 'ROLLBACK' query on the global $tekodb database connection.
     *
     * @return void
     */
    public static function rollBack() {
        global $tekodb;
        $tekodb->query('ROLLBACK');
    }

    /**
     * Begins a new transaction on the global $tekodb database connection.
     *
     * @return void
     */
    public static function beginTransaction() {
        global $tekodb;
        $tekodb->query('BEGIN');
    }
}