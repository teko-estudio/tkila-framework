<?php

namespace Tkila\Database;

use Tipsy\Request;
use Tkila\Database\Exceptions\ModelNotFoundException;
use Tkila\Database\Traits\Paginate;
/**
 * Class Model
 * @method Model where($key, $operator = null, $value = null)
 * 
*/


abstract class Model
{
    use Paginate;
    
    /**
     * Nombre de la tabla con la que esta asociada el modelo.
     *
     * @var string
     */
    protected $table = '';
    /**
     * Campos que se usaran para guardar o actualizar algun modelo
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * Campos que ocultará por default al hacer una consulta sobre el model
     *
     * @var array
     */
    protected $hidden = [];
    /**
     * Campos que mostrará por default al hacer una consulta sobre el model
     *
     * @var array
     */

    protected $visible = ['*'];

    /**
     * Nombre de la llave primaria
     * 
     * @var string
     */
    protected $keyName = "id";

    /**
     * Utils
     */
    private $select = "";

    private $group = "";

    private $having = "";

    private Request | null $Request = null;

    private $props = [];

    protected $timestamps = false;

    public function __construct($props = [])
    {
        $this->initSelect();
        foreach ($props as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function initSelect() {
        $this->select = "SELECT {$this->getVisible()} FROM {$this->table} ";
    }

    /**
     * Sets the columns to be selected in the query or the query string.
     *
     * @param mixed $columns The columns or query to be selected. Can to be an array or a query string.
     * @return $this The current instance of the class.
     */
    public function select($columns)
    {
        if(is_array($columns)) {
            $this->select = "SELECT " . implode(", ", $columns) . " FROM {$this->table} ";
        } else {
            $this->select = $columns;
        }
        return $this;
    }

    /**
     * Sets the query for the object and returns the current instance.
     *
     * @param string $query The query to set for the object.
     * @return $this The current instance of the object.
     */
    public function query($query) {
        $this->select = $query;
        return $this;
    }

    /**
     * Retrieves a record from the database based on the given ID.
     *
     * @param int $id The ID of the record to retrieve.
     * @return static|null An instance of the class representing the retrieved record, or null if no record is found.
     */
    public function getById($id)
    {
        global $tekodb;
        $row =  $tekodb->get_row($this->select . " WHERE " . $this->getKeyName() . " = '{$id}'");
        if ($row) {
            return new static((array)$row);
        }

        return null;
    }

    /**
     * Retrieves a row from the database table based on a specific field and value.
     *
     * @param string $field The field to search for.
     * @param mixed $value The value to search for.
     * @return static|null The retrieved row from the database, or null if not found.
     */
    public function getBy($field, $value)
    {
        global $tekodb;
        $row = $tekodb->get_row($this->select . " WHERE " . $field . " = '{$value}'");
        if ($row) {
            return new static((array)$row);
        }

        return null;
    }

    /**
     * Update a row or rows from the database table based on a specific field and value.
     * 
     * @param array $data The data to be updated.
     * @param string $id The id of the row to be updated.
     * @param string|null $keyName The name of the key to be used in the where clause if is null this use model config.
     * @param array $otherFilters The other filters to be used in the where clause this is you want validate with other filters example ['created_by' => '1'].
     */
    public function update($data, $id, $keyName = null, $otherFilters = [])
    {
        global $tekodb;
        $safe_data = [];
        $data = (array)$data;
        $key = $keyName ?? $this->getKeyName();
        $fillable = !empty($this->fillable) ? $this->getFillable() : array_keys($data);
        foreach ($fillable as $column) {
            //Validamos si los valores son validos
            if (isset($data[$column])) {
                $safe_data[$column] = $data[$column] === '' ? NULL : trim($data[$column]);
            }

            if ($this->timestamps) {
                $safe_data['updated_at'] = date('Y-m-d H:i:s');
            }
        }

        if(empty($safe_data)) return false;

        $updated = $tekodb->update($this->table, $safe_data, [$key => $id, ...$otherFilters]);
        return $updated;
    }

    /**
     * Delete a row or rows from the database table based on a specific field and value.
     * @param string $id The id of the row to be deleted.
     * @param string|null $keyName The name of the key to be used in the where clause if is null this use model config.
     * @param array $otherFilters The other filters to be used in the where clause this is you want validate with other filters example ['created_by' => '1'].
     */
    public function delete($id = null, $keyName = null, $otherFilters = [])
    {
        global $tekodb;
        $key = $keyName ?? $this->getKeyName();
        if ($id) {
            return $tekodb->delete($this->table, [$key => $id, ...$otherFilters]);
        } else if (isset($this->{$key})) {
            //Permite borrar a traves de la entidad
            return $tekodb->delete($this->table, [$key => $this->{$key}]);
        }
    }

    /**
     * Soft deletes a record by setting the 'deleted_at' timestamp.
     *
     * @param mixed $id The ID of the record to be soft deleted.
     * @param string|null $keyName The name of the primary key column.
     * @param array $otherFilters Additional filters for the soft delete operation.
     * @return void
     */
    public function softDelete($id = null, $keyName = null, $otherFilters = [])
    {
        global $tekodb;
        $key = $keyName ?? $this->getKeyName();
        if ($id) {
            return $tekodb->update($this->table, [
                'deleted_at' => date('Y-m-d H:i:s')
            ], [$key => $id, ...$otherFilters]);
        } else if (isset($this->{$key})) {
            return $tekodb->update($this->table, [
                'deleted_at' => date('Y-m-d H:i:s')
            ], [$key => $id, ...$otherFilters]);
        }
    }

    public function having() {
        $args = func_get_args();
        $statement = empty($this->having) ? ' HAVING  ' : ' AND ';
        $validation = match (count($args)) {
            1 => function () use ($args) {
                // this is like addWhereStatement("id = 1")
                [$raw] = $args;
                if (is_array($raw)) {
                    return implode(' AND ', $raw);
                } else {
                    return " {$raw} ";
                }
            },
            2 => function () use ($args) {
                // this is like addWhereStatement("id", 1)
                [$column, $value] = $args;
                return " {$column} = " . $this->formatValueByType($value);
            },
            3 => function () use ($args) {
                // this is like addWhereStatement("id", "<>", 1)
                [$column, $operator, $value] = $args;
                return " {$column} {$operator} " . $this->formatValueByType($value);
            }
        };
        $statement .= $validation();
        $this->having .= $statement;
        return $this;
    }

    /**
     * Saves the given data into the database table.
     *
     * @param mixed $data The data to be saved.
     * @return bool Returns `true` if the data was successfully saved, `false` otherwise.
     */
    public function save($data = [])
    {
        global $tekodb;
        $safe_data = [];
        $data = empty($data) ? $this->props : (array)$data;
        $fillable = !empty($this->fillable) ? $this->fillable : array_keys($data);
        // Limpiamos los datos sobre los configurados en la clase
        foreach ($fillable as $key) {
            //Validamos si los valores son validos
            if (isset($data[$key])) {
                $safe_data[$key] = $data[$key] === '' ? null : trim($data[$key]);
            }

            if ($this->timestamps) {
                $safe_data['created_at'] = date('Y-m-d H:i:s');
            }
        }

        //Guardamos el resutado del INSERT (TRUE / FALSE)
        $inserted = $tekodb->insert($this->table, $safe_data);
        // Si es tru creamos propiedades para los datos insertados
        if ($inserted) {
            foreach ($safe_data as $key => $value) {
                $this->{$key} = $value;
            }

            //Si en los dato no viene el id lo agregamos esto deberia de pasar con AUTO_INCREMENT
            if (!isset($this->{$this->getKeyName()})) {
                $this->{$this->getKeyName()} = $tekodb->insert_id;
            }
        }
        //Retornamos (TRUE / FALSE)
        return $inserted;
    }


    /**
     * Creates a new instance of the class and saves it to the database.
     * @param array|object $data The data to be saved.
     */
    public static function create($data)
    {
        $instance = static::getInstance();
        $instance->save($data);
        return $instance;
    }


    /**
     * Retrieves the last inserted ID from the database.
     *
     * @return mixed The last inserted ID.
     */
    public function lastId()
    {
        global $tekodb;
        return $tekodb->insert_id;
    }

    /**
     * Fetches data from the database based on the specified criteria.
     *
     * @return array The fetched data.
     */
    public function get()
    {
        global $tekodb;
        $results = $tekodb->get_results("{$this->select} {$this->where} {$this->group} {$this->having} {$this->sort} {$this->limit}") ?? [];
        return $results;
    }

    /**
     * Retrieves the first row from the database query result.
     *
     * @return static|null The first row from the query result as an instance of the current class, or null if no result is found.
     */
    public function first() {
        global $tekodb;
        $result = $tekodb->get_row("{$this->select} {$this->where} {$this->group} {$this->having} {$this->sort} LIMIT 1") ?? null;
        return is_null($result) ? null : new static((array)$result);
    }


    /**
     * Retrieves the first value from the database query result.
     *
     * @return mixed|null The first value from the query result, or null if no result is found.
     */
    public function first_var() {
        global $tekodb;
        // Limit added just to be sure
        $result = $tekodb->get_var("{$this->select} {$this->where} {$this->group} {$this->having} {$this->sort} LIMIT 1") ?? null;
        return $result;
    }

    /**
     * Show the select as string is an util to logging the query.
     */
    public function getSql()
    {
        return "{$this->select} {$this->where} {$this->group} {$this->having} {$this->sort} {$this->limit}";
    }

    /**
     * Retrieves the specified data from the database based on the provided key name.
     *
     * @param string $keyName The name of the key to retrieve the data from.
     * @return array The array of retrieved data based on the provided key name.
     */
    public function take($keyName)
    {
        global $tekodb;
        $rows = $tekodb->get_results("{$this->select} {$this->where} {$this->group} {$this->having} {$this->sort} {$this->limit}", ARRAY_A) ?? [];
        return array_column($rows, $keyName);
    }

    /**
     * Set the GROUP BY clause for the SQL query.
     *
     * @param string $keyName The name of the column to group by
     * @return $this
     */
    public function groupBy($keyName)
    {
        if(is_array($keyName)) {
            $keyName = implode(', ', $keyName);
        }
        $this->group .= empty($this->group) ? ' GROUP BY ' . $keyName . ' ' : ', ' . $keyName . ' ';
        return $this;
    }

    /**
     * A method to load and assign values to class properties.
     *
     * @return void
     */
    public function load(): void
    {
        $arguments = func_get_args();
        for ($i = 0; $i < count($arguments); $i++) {
            if (is_array($arguments[$i])) {
                $is_assoc = array_keys($arguments[$i]) !== range(0, count($arguments[$i]) - 1);
                //If is assoc we pass value like parameters to function
                if ($is_assoc) {
                    foreach ($arguments[$i] as $key => $value) {
                        if(!is_array($value)) $value = [$value];
                        $this->{$key} = method_exists($this, $key) ? $this->{$key}(...$value) : null;
                    }
                } else {
                    foreach ($arguments[$i] as $value) {
                        $this->{$value} = method_exists($this, $value) ? $this->{$value}() : null;
                    }
                }
            } else {
                $this->{$arguments[$i]} = method_exists($this, $arguments[$i]) ? $this->{$arguments[$i]}() : null;
            }
        }
    }


    /**
     * Retrieves all the results from the database.
     *
     * @return array The array of results from the database.
     */
    public function all()
    {
        global $tekodb;
        $results = $tekodb->get_results($this->select);
        return $results ?? [];
    }

    /**
     * Joins the current table with the specified table based on the given condition.
     *
     * @param string $table The name of the table to join.
     * @param string $on The join condition.
     * @param string $type The type of join (default: 'INNER').
     * @return $this The current instance of the class.
     */
    public function join($table, $on, $type = 'INNER') {
        $this->select .= " {$type} JOIN {$table} ON {$on} ";
        return $this;
    }

    /**
     * Sets the key name.
     *
     * @param string $keyName The new key name.
     * @return $this The current object instance.
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;
        return $this;
    }

    /**
     * Get the key name.
     *
     * @return string The key name.
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * Sets the visibility of the object.
     *
     * @param mixed $visible The visibility options. Default is ['*'].
     * @return $this The current object instance.
     */
    public function setVisible($visible = ['*'])
    {
        $this->visible = $visible;
        $this->select = "SELECT {$this->getVisible()} FROM {$this->table} ";
        return $this;
    }

    /**
     * Retrieves the visible fields as a comma-separated string.
     *
     * @return string The visible fields as a comma-separated string.
     */
    public function getVisible()
    {
        $fields = implode(",", array_filter($this->visible, function ($value) {
            return !in_array($value, $this->hidden);
        }));
        return $fields;
    }


    /**
     * Sets the value of the fillable property.
     *
     * @param array $fields The new value of the fillable property.
     */
    public function setFillable($fillable)
    {
        $this->fillable = $fillable;
        return $this;
    }

    /**
     * Retrieves the fillable field of the table.
     *
     * @return array The fillable of the table.
     */
    public function getFillable()
    {
        return $this->fillable;
    }

    /**
     * Retrieves the properties of the object.
     *
     * @return array The properties of the object.
     */
    public function toArray()
    {
        return $this->props;
    }

    /**
     * Retrieves the table name.
     */
    public function getTableName()
    {
        return $this->table;
    }

    /** 
     * Creates a new instance of the class.
     */
    public static function getInstance()
    {
        return new static();
    }

    /**
     * Sets the value of a property dynamically.
     *
     * @param string $name The name of the property.
     * @param mixed $value The value to set for the property.
     */
    public function __set($name, $value)
    {
        $this->props[$name] = $value;
    }

    /**
     * A description of the entire PHP function.
     *
     * @param string $name
     */
    public function __get($name)
    {
        return $this->props[$name];
    }

    /**
     * Magic method to handle dynamic method calls.
     *
     * @param string $method The method being called
     * @param array $arguments The method arguments
     * @throws \Exception Method not found
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return match ($method) {
            'where' => $this->addWhereStatement(...$arguments),
            default => throw new \Exception('Method not found')
        };
    }


    /**
     * Calls a static method on the class if it exists, otherwise calls the method on a new instance of the class.
     *
     * @param string $name The name of the method to call.
     * @param array $arguments The arguments to pass to the method.
     * @return mixed The result of the method call.
     */
    public static function __callStatic($name, $arguments)
    {
        return (new static)->{$name}(...$arguments);
    }
}
