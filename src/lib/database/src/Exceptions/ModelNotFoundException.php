<?php
namespace Tkila\Database\Exceptions;

use Exception;

class ModelNotFoundException extends Exception { }