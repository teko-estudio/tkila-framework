<?php

namespace Tkila\Database\Traits;

use Tipsy\Request;

trait Paginate
{
    private $where = "";
    private $sort = "";
    private $limit = "";


    private function instatiateRequest()
    {
        if (!$this->Request) {
            $this->Request = new Request();
        }
    }

    public function toTable()
    {
        return [$this->applyFilters()->applyPaginate()->applySort()->get(), $this->total()];
    }

    /**
     * Apply filters to the data.
     *
     * @param array|null $filters Optional. An array of filters to apply. Default is null.
     * @return $this The current object.
     */
    public function applyFilters(array|null $filters = null)
    {
        $this->instatiateRequest();
        $filters = $filters ?? $this->Request->filters;
        $filter_search = $this->Request->filter_search;
        $filter_columns = $this->Request->filter_columns;
        if (!empty($filter_search) && !empty($filter_columns)) {
            foreach ($filter_columns as $column) {
                $filters_query[] = "$column LIKE  '%$filter_search%'";
            }
            $this->addWhereStatement("(" . implode(' OR ', $filters_query) . ")");
        }

        if (!empty($filters)) {
            $where = array();
            foreach ($this->Request->filters as $filter) {
                if ($filter['value'] == 0 || !empty($filter['value'])) {
                    $function = "filter" . ucwords($filter['key']);
                    if (method_exists($this, $function)) {
                        $result = $this->$function($filter['value']);
                        if (!empty($result))
                            $where[] = $result;
                    } else {
                        if (!empty($filter['like'])) {
                            $where[] = match ($filter['like']) {
                                'to_right'      => "{$filter['key']} LIKE '{$filter['value']}%'",
                                'to_left'       => "{$filter['key']} LIKE '%{$filter['value']}'",
                                'to_same'       => "{$filter['key']} = " . $this->formatValueByType($filter['value']),
                                default         => "{$filter['key']} LIKE '%{$filter['value']}%'"
                            };
                        } else if (!empty($filter['operation'])) {
                            $value = $this->formatValueByType($filter['value']);
                            if (!$value) continue;
                            $temp_where = match (true) {
                                $filter['operation'] === '>'  => "{$filter['key']} > {$value}",
                                $filter['operation'] === '<'  => "{$filter['key']} < {$value}",
                                $filter['operation'] === '>=' => "{$filter['key']} >= {$value}",
                                $filter['operation'] === '<=' => "{$filter['key']} <= {$value}",
                                $filter['operation'] === 'between' && is_array($filter['value']) && count($filter['value']) === 2 => "{$filter['key']} BETWEEN '{$filter['value'][0]}' AND '{$filter['value'][1]}'", 
                                default => null
                            };
                            if (!$temp_where) continue;
                            $where[] = $temp_where;
                        } else {
                            if (is_array($filter['value'])) {
                                $value = $this->formatValueByType($filter['value']);
                                $where[] = "{$filter['key']} IN {$value}";
                            } else {
                                $where[] = "{$filter['key']} LIKE '%{$filter['value']}%'";
                            }
                        }
                    }
                }
            }
            if (!empty($where)) $this->addWhereStatement($where);
        }

        return $this;
    }

    /**
     * Adds a WHERE statement to the SQL query.
     *
     * @param array|string $where The WHERE statement to be added or field to compare.
     * @param string $value|$operator to apply where or operator to compare.
     * @param string $value to apply where or operator to compare.
     * @return $this
     */
    public function addWhereStatement()
    {
        $args = func_get_args();
        $statement = empty($this->where) ? ' WHERE ' : ' AND ';
        $validation = match (count($args)) {
            1 => function () use ($args) {
                // this is like addWhereStatement("id = 1")
                [$where] = $args;
                if (is_array($where)) {
                    return implode(' AND ', $where);
                } else {
                    return " {$where} ";
                }
            },
            2 => function () use ($args) {
                // this is like addWhereStatement("id", 1)
                [$where, $value] = $args;
                return " {$where} = " . $this->formatValueByType($value);
            },
            3 => function () use ($args) {
                // this is like addWhereStatement("id", "<>", 1)
                [$where, $operator, $value] = $args;
                return " {$where} {$operator} " . $this->formatValueByType($value);
            }
        };
        $statement .= $validation();
        $this->where .= $statement;
        return $this;
    }

    public function conditionValidation() {
        
    }

    /**
     * Apply pagination to the result of a query.
     *
     * @return $this
     */
    public function applyPaginate()
    {
        $this->instatiateRequest();
        $pagination = $this->Request->pagination;
        $this->limit = "";
        if (!empty($pagination)) {
            $currentPage = intval($pagination["page"]);
            $perPage = intval($pagination["rowsPerPage"]);
            $start = ($currentPage - 1) * $perPage;
            $this->limit = " LIMIT {$start},{$perPage}";
            if($perPage == 0) $this->limit = "";
        }

        return $this;
    }


    /**
     * Apply sorting to the data based on pagination settings.
     *
     * @return $this
     */
    public function applySort()
    {
        $this->instatiateRequest();
        $pagination = $this->Request->pagination;
        if (!empty($pagination)) {
            $key = $pagination["sortBy"];
            if (!boolval($key)) return $this;
            $descending = boolval($pagination["descending"]);
            $this->sortBy($key, $descending);
        }

        return $this;
    }

    public function sortBy($key, $descending = false)
    {
        $this->sort .= empty($this->sort) ? " ORDER BY {$key} " . ($descending ? "DESC" : "ASC") : " , {$key} " . ($descending ? "DESC" : "ASC");
        return $this;
    }


    /**
     * Retrieves the total value from the database.
     *
     * @return int The total value.
     */
    public function total()
    {
        global $tekodb;
        $total = $tekodb->get_var("
            SELECT COUNT(*)
            FROM ({$this->select} {$this->where} {$this->group} {$this->having}) AS total; 
        ");
        return intval($total);
    }

    /**
     * Formats a value based on its type.
     *
     * @param mixed $value The value to be formatted.
     * @return mixed The formatted value.
     */
    private function formatValueByType($value)
    {
        return match (true) {
            is_string($value)   => "'{$value}'",
            is_numeric($value)  => $value,
            is_array($value)    => "('" . implode("','", $value) . "')",
            default => null
        };
    }
}
