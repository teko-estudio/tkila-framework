<?php

use Gettext\Translator\TranslatorFunctions;

/**
 * Returns the translation of a string.
 */
function __(string $original, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->gettext($original);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}

/**
 * Noop, marks the string for translation but returns it unchanged.
 */
function noop__(string $original, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->noop($original);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}

/**
 * Returns the singular/plural translation of a string.
 */
function n__(string $original, string $plural, int $value, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->ngettext($original, $plural, $value);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}

/**
 * Returns the translation of a string in a specific context.
 */
function p__(string $context, string $original, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->pgettext($context, $original);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}

/**
 * Returns the translation of a string in a specific domain.
 */
function d__(string $domain, string $original, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->dgettext($domain, $original);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}

/**
 * Returns the translation of a string in a specific domain and context.
 */
function dp__(string $domain, string $context, string $original, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->dpgettext($domain, $context, $original);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}

/**
 * Returns the singular/plural translation of a string in a specific domain.
 */
function dn__(string $domain, string $original, string $plural, int $value, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->dngettext($domain, $original, $plural, $value);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}

/**
 * Returns the singular/plural translation of a string in a specific context.
 */
function np__(string $context, string $original, string $plural, int $value, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->npgettext($context, $original, $plural, $value);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}

/**
 * Returns the singular/plural translation of a string in a specific domain and context.
 */
function dnp__(string $domain, string $context, string $original, string $plural, int $value, ...$args): string
{
    $text = TranslatorFunctions::getTranslator()->dnpgettext($domain, $context, $original, $plural, $value);
    return TranslatorFunctions::getFormatter()->format($text, $args);
}
