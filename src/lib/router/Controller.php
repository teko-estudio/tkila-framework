<?php
namespace Tkila\Routing;


abstract class Controller {
    public function __construct(protected Response $response = new Response()){ }
    public function getResponse() { return $this->response->getResponse(); }
}