<?php
namespace Tkila\Routing;

use Exception;
use ReflectionMethod;
use Throwable;
use Tkila\Database\Exceptions\ModelNotFoundException;
use Tkila\Routing\Exceptions\ValidationErrorException;
/*
 * @method static post(string $http_method, string $base_path, array $controller)
 */
class Router {
    public static $routes = [];
    private static $previous_base_paths = [];

    /**
     * Adds a group to the base path and executes the given action.
     *
     * @param string $base The base path to be added to the group.
     * @param mixed $action The action to be executed. If it is a string, it is treated as a file path and required. If it is a callable, it is directly executed.
     * @return void
     */
    public static function AddGroup($base, $action){
        static::$previous_base_paths[] = "/".trim($base, "/");
        if(is_string($action)){
            require_once($action);
        }else {
            $action();
        }
        array_pop(static::$previous_base_paths);
    }

    /**
     * Adds a route to the application's routing table.
     *
     * @param string $http_method The HTTP method for the route.
     * @param string $base_path The base path for the route.
     * @param array $controller The controller for the route.
     * @return void
     */
    public static function AddRoute(string $http_method, string $base_path, array $controller){
        [$class_name, $method] = $controller;
        $path = implode("",static::$previous_base_paths) . "/" . trim($base_path, "/");
        if(!array_key_exists($path, static::$routes)){
            static::$routes[$path][] = [
                "http_method"       => strtolower($http_method),
                "controller"        => $class_name,
                "method"            => $method
            ];
        }else {
            static::$routes[$path][] = [
                "http_method"       => strtolower($http_method),
                "controller"        => $class_name,
                "method"            => $method
            ];
        }
        
    }

    public static function __callStatic($name, $arguments)
    {
        match($name){
            'post' => static::AddRoute('post', ...$arguments),
            'get' => static::AddRoute('get', ...$arguments),
            'put' => static::AddRoute('put', ...$arguments),
            'delete' => static::AddRoute('delete', ...$arguments),
            'group' => static::AddGroup(...$arguments),
            default => throw new Exception('Method not found')
        };
        
    }

    /**
     * Collects the routes and registers them with the global router.
     *
     * @throws Exception If the response from the controller method is invalid.
     */
    public static function Collect(){
        global $router;
        foreach(static::$routes as $route => $route_data){
            foreach($route_data as $data){
                $router->when([
                    "route" => trim($route, '/'),
                    "method" => $data["http_method"],
                    "controller" => function($Request, $View, $Scope, $Params) use ($data){
                            try{
                                $instance = new $data['controller']();
                                $reflection = new ReflectionMethod($instance, $data['method']);
                                $parameters = $reflection->getParameters();
                                $request = $Request;
                                if(count($parameters) > 0){
                                    $request_type = $parameters[0]->getType()->getName();
                                        //Inicializamos el $request en base al $Request de Typsy
                                        $request = new $request_type($Request);
                                        //Solo si el parametro tiene un request especifico validamos de una las reglas
                                        if($request instanceof Request && $request_type !== Request::class){
                                            $request->validate();
                                        }
                                }

                                $controllerParams = [
                                    'request' => $request,
                                    'view' => $View,
                                    'scope' => $Scope,
                                    'params' => $Params
                                ];

                                $request_params = [];
                                foreach($parameters as $param) {
                                    $temp = strtolower($param->getName());
                                    if(array_key_exists($temp, $controllerParams)){
                                        $request_params[] = $controllerParams[$temp];
                                    }
                                }

                                // Recibir parametros en cualquier orden
                                $res = $instance->{$data['method']}(...$request_params);

                                //Obtenemos el response siempre deberia de ser un array
                                if($res instanceof Response || $res instanceof Controller) {
                                    teko_json($res->getResponse());
                                } else if (is_array($res)){
                                    teko_json($res);
                                }

                                //Si no es un array o un response lanzamos un error
                                throw new Exception("Invalid response.");
                            }catch(ValidationErrorException $e){
                                teko_json(['error' => true, 'message' => $e->getMessage()]);
                            } catch (ModelNotFoundException $e) {
                                teko_json(['error' => true, 'message' => $e->getMessage()]);
                            } catch (\PDOException $e) {
                                $code = $e->getCode();
                                if ($code == '23000') {
                                    $errorMessage = $e->getMessage();
                                    if (str_contains(strtolower($errorMessage), 'foreign key') !== false) {
                                        teko_json(['error' => true, 'message' => 'Ha ocurrido un error al eliminar o actualizar por que el registro esta relacionado en otras tablas', 'exception' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
                                    } else if (strpos(strtolower($errorMessage), 'duplicate entry') !== false) {
                                        teko_json(['error' => true, 'message' => 'Ya existe un registro con este valor unico', 'exception' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
                                    }
                                }
                                teko_json(['error' => true, 'message' => 'Ocurrion un erro con la base de datos code: '. $code, 'exception' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
                            } catch (Throwable $e) {
                                teko_json(['error' => true, 'message' => 'Ha ocurrido un error inesperado', 'exception' => $e->getMessage(), 'trace' => $e->getTraceAsString()]);
                            }
                        }
                    ]
                );
            }
        }   
    }
}
