<?php
namespace Tkila\Routing;

class Response {
    private $error = false;
    private $message = false;
    private $data = null;
    private $custom_data = [];
    private $total = null;

    /**
     * Sets the data for the function.
     *
     * @param mixed $data The data to be set.
     * @return $this
     */
    final public function data(mixed $data) {
        $this->data = $data;
        return $this;
    }

    /**
     * Sets the custom data for the object.
     *
     * @param array $data The custom data to be set.
     * @return $this The current object.
     */
    final public function customData(array $data) {
        $this->custom_data = $data;
        return $this;
    }

    /**
     * Sets the total data tables
     *
     * @param int $total The total value to be set.
     * @return $this The current object.
     */
    final public function total(int $total) {
        $this->total = $total;
        return $this;
    }

    /**
     * Sets the error value of the class.
     *
     * @param bool $value The new error value.
     * @return $this The current object instance.
     */ 
    final public function error(bool $value) {
        $this->error = $value;
        return $this;
    }

    /**
     * Sets the message value for the function.
     *
     * @param mixed $value The value to set as the message.
     * @return $this The current instance of the class.
     */
    final public function message($value) {
        $this->message = $value;
        return $this;
    }

    /**
     * Retrieve the response data.
     *
     * @return array The response data.
     */
    final public function getResponse() {
        return [
            "error" => $this->error,
            "message" => $this->message,
            "data" => $this->data,
            "total" => $this->total,
            ...$this->custom_data
        ];
    }
}