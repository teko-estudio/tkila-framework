<?php
namespace Tkila\Routing;

use Rakit\Validation\Validation;
use Rakit\Validation\Validator;
use Tipsy\Request as TipsyRequest;
use Tkila\Routing\Exceptions\ValidationErrorException;

class Request {
    
    private $request = [];
    protected $rules = [];
    private $validatedData = [];
    protected $messages = [];

    private Validation | null $validation = null;

    /**
     * Constructor for the class.
     *
     * @param mixed $Request The request object.
     * @return void
     */
    public function __construct($Request = null) {
        if($Request && $Request instanceof TipsyRequest){
            $this->request = $Request->request();
        }
    }

    /**
     * Sets the rules for the function.
     *
     * @param array $rules The rules to be set.
     * @return $this The current object.
     */
    public function rules(array $rules) {
        $this->rules = $rules;
        return $this;
    }
    
    /**
     * Sets the messages for the object.
     *
     * @param array $messages The messages to set.
     * @return $this The current object.
     */
    public function messages(array $messages) {
        $this->messages = $messages;
        return $this;
    }

    /**
     * Validates the data using the given rules and throws a ValidationErrorException if validation fails.
     *
     * @throws ValidationErrorException description of exception
     * @return $this
     */
    public function validate() {
        $this->validatedData = [];
        $validator = new Validator($this->messages);
        $this->validation = $validator->make($this->request, $this->rules);

        $this->validation->validate();

        if($this->validation->fails()){
            throw new ValidationErrorException($this->validation->errors()->all()[0]);
        }

        foreach($this->validation->getValidatedData() as $key => $value){
            $this->validatedData[$key] = $value;
        }

        return $this;

    }

    /**
     * Retrieves the parameters of the function.
     *
     * @return array The array of parameters.
     */
    public function getParams(): array {
        return $this->request;
    }

    /**
     * Retrieves the validated data.
     *
     * @return array The validated data.
     */
    public function getValidatedData() : array {
        return $this->validatedData;
    }

    /**
     * Retrieves the value of a specified property.
     *
     * @param string $name The name of the property to retrieve.
     * @return mixed The value of the specified property.
     */
    public function __get($name) {
        return $this->validatedData[$name];
    }
}